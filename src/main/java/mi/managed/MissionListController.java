/**
 * BE Java EE "Mission Impossible"
 * Mineure "développement web"
 * CentraleSupélec, campus de Rennes
 * 
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 * 2016
 */
package mi.managed;

import java.util.ArrayList;
import javax.ejb.EJB;
import javax.inject.Named;
import mi.missions.EtatMission;
import mi.agents.AccountRemote;
import mi.missions.ListeMissions;
import mi.missions.Mission;
import mi.missions.TraitementMissionsRemote;

/**
 * "Managed bean" pour accéder à la liste des missions.
 *
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 */
@Named("missionListController")
public class MissionListController {
    //TODO Il manquera très certainement des méthodes dans cette classe.
    
    /**
     * EJB encapsulant toutes les missions connues du système.
     */
    @EJB
    private ListeMissions missions;
    
    /**
     * EJB proposant l'API de traitement des missions.
     */
    @EJB
    private TraitementMissionsRemote tm;

    
    /**
     * EJB encapsulant le compte de l'utilisateur connecté.
     */
    @EJB
    private AccountRemote account;
    
    /**
     * Constructeur par défaut. 
     */
    public MissionListController() {
    }
    
    /**
     * Récupération de l'ensemble des missions.
     * @return Liste de toutes les missions connues du système.
     */
    public ArrayList<Mission> getMissions() {
        return this.missions.getMissions();
    }
   
    public ArrayList<Mission> getMissionsParAgent() {
        return this.missions.missionsParAgent(this.account.getUsername());
    }
    
    public ArrayList<Mission> getMissionsParAdmin() {
        return this.missions.missionsSuiviesPar(this.account.getUsername()); 
    }
    
    /**
     * Récupération de l'ensemble des missions en état demandée.
     * @return Liste de toutes les missions demandées connues du système.
     */
    public ArrayList<Mission> getMissionDemandees() {
        return this.missions.getMissionsDemandees();
    }
    /**
     * Introduction d'une nouvelle mission dans le système.
     * @param m La mission à introduire dans le système (elle pourra être ou ne 
     * pas être clonée).
     */

    public String creerMission(Mission m) {
        this.missions.ajouterMission(m);
        return "/agents/index.xhtml";
    }
    
}
